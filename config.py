import os

# You need to replace the next values with the appropriate values for your configuration

basedir = os.path.abspath(os.path.dirname(__file__))
SQLALCHEMY_ECHO = False
SQLALCHEMY_TRACK_MODIFICATIONS = True
# SQLALCHEMY_DATABASE_URI = "postgresql://postgres:postgres@localhost/postgres"

# SQLALCHEMY_DATABASE_URI = "postgresql+psycopg2://postgres:postgres@/postgres?host=/cloudsql/visiondetector-246111:asia-south1:sample"
# SQLALCHEMY_DATABASE_URI = "postgresql+psycopg2://postgres:postgres@35.200.241.66/postgres"

DATA_BACKEND = 'cloudsql'

# Google Cloud Project ID. This can be found on the 'Overview' page at
# https://console.developers.google.com
PROJECT_ID = 'visiondetector-246111'

# CloudSQL & SQLAlchemy configuration
CLOUDSQL_USER = 'postgres'
CLOUDSQL_PASSWORD = 'postgres'
CLOUDSQL_DATABASE = 'postgres'
CLOUDSQL_CONNECTION_NAME = 'visiondetector-246111:asia-south1:sample'
LOCAL_SQLALCHEMY_DATABASE_URI = (
    'postgresql+psycopg2://{user}:{password}@127.0.0.1:3306/{database}').format(
        user=CLOUDSQL_USER, password=CLOUDSQL_PASSWORD,
        database=CLOUDSQL_DATABASE)

# When running on App Engine a unix socket is used to connect to the cloudsql
# instance.
LIVE_SQLALCHEMY_DATABASE_URI = (
    'postgres://{user}:{password}@localhost/{database}'
    '?host=/cloudsql/{connection_name}').format(
        user=CLOUDSQL_USER, password=CLOUDSQL_PASSWORD,
        database=CLOUDSQL_DATABASE, connection_name=CLOUDSQL_CONNECTION_NAME)

if os.environ.get('GAE_INSTANCE'):
    SQLALCHEMY_DATABASE_URI = LIVE_SQLALCHEMY_DATABASE_URI
else:
    SQLALCHEMY_DATABASE_URI = LOCAL_SQLALCHEMY_DATABASE_URI